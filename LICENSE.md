Copyright 2016--2018 Jordan Watts and the authors of the Fishing Project.

Permission is granted to copy, distribute and/or modify this document under the terms of the Creative Commons Attribution 4.0 International License or any later version published by the Creative Commons Corporation (“Creative Commons”).
A copy of the license is included in the folder "licenses" and the section entitled "Creative Commons Attribution 4.0 International License".

The section "The category of groupoids" is licensed under the terms of the Creative Commons Attribution NonCommercial NoDerivatives 4.0 International License.
A copy of the license is included in the folder "licenses" and the section entitled "Creative Commons Attribution NonCommercial NoDerivatives 4.0 International License".