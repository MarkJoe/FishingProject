LISCIART = FishingProject


pdf : $(LISCIART).tex 
	pdflatex $(LISCIART)	

bib : $(LISCIART).tex
	latex $(LISCIART).tex
	bibtex $(LISCIART)
	latexml --bibtex $(HOME)/texmf/bibtex/bib/lmlib > lmlib.xml
	mv $(LISCIART).aux tmp/ & mv $(LISCIART).bbl tmp/ & mv $(LISCIART).blg tmp/ & mv $(LISCIART).log tmp/ & mv $(LISCIART).dvi tmp/ 

xml : $(LISCIART).tex
	latexml --path=$(HOME)/texmf/tex/libermathstyle --includestyles --dest=$(LISCIART).xml $(LISCIART).tex

xhtml : $(LISCIART).xml 
	latexmlpost --novalidate --css=theme-blue.css --css=amsart.css --bibliography=lmlib.xml --dest=$(LISCIART).xhtml $(LISCIART).xml

diagrams : $(LISCIART).xhtml
	mv $(LISCIART).xhtml $(LISCIART).tmp.xhtml
	sed -f diagram.rules $(LISCIART).tmp.xhtml > $(LISCIART).xhtml
	rm $(LISCIART).tmp.xhtml

lmstyle : $(LISCIART).xhtml
	mv $(LISCIART).xhtml $(LISCIART).tmp.xhtml
	sed -f lmstyle.rules $(LISCIART).tmp.xhtml > $(LISCIART).xhtml
	rm $(LISCIART).tmp.xhtml

all : pdf xml xhtml diagrams lmstyle

clean : 
	rm tmp/*.aux & rm tmp/*.log & rm tmp/*.dvi 
