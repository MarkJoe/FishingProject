% Copyright 2016 - 2018 Markus J. Pflaum
% main author: 
%   Markus J. Pflaum 
%
% Permission is granted to copy, distribute and/or modify this document under the terms of the 
% Creative Commons Attribution 4.0 International License or any later version published by the 
% Creative Commons Corporation (“Creative Commons”) or under the terms of the 
% GNU Free Documentation License, Version 1.3 or any later version published by 
% the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
% Copies of the licenses are included in the folder license.
%
\section{Charts, atlases, and manifolds}\label{sec:charts-atlases-manifolds}

\para Manifolds are topological spaces which are locally modeled on 
euclidean space by homeomorphisms called charts. 
If such a manifold is covered by charts in a way that the transition maps 
between these charts all lie in  a particular differentiability class one obtains a differentiable 
manifold. Let us make this more precise. 

\begin{definition}
  Assume that $\fldk$ is the field of real or complex numbers, and $M$ a topological space.
  By a \emph{chart} of $M$ one understands a homeomorphism
  $x: U \rightarrow \widetilde{U}$ from an open subset $U \subset M$ onto an open 
  subset $\widetilde{U}$ of some $\fldk^n$. We often denote a 
  chart of $M$ by $(x,U,\fldk^n)$ or just shortly by $(x,U)$, and call
  the chart \emph{real} respectively  \emph{complex} depending on whether
  $\fldk =\R$ or  $\fldk =\C$.
  The number $n$ is called the 
  \emph{real} respectively the \emph{complex} \emph{dimension} of the chart. 
  
  Let $k \in \N\cup \{ \infty, \omega\}$. Two real charts 
  $x: U \rightarrow \widetilde{U} \subset \R^n$ and 
  $y: V \rightarrow \widetilde{V} \subset \R^m$ are said to be  $\shContFcts^k$-\emph{compatible}
  if the transition maps $x\circ y^{-1} : y(U\cap V) \to x(U\cap V) $ and 
  $y \circ x^{-1} : x(U\cap V) \to y(U\cap V) $ are both of class $\shContFcts^k$,
  which means continuous if $k=0$, $k$-times continuously differentiable if $k\in \N^*$,
  smooth if $k=\infty$, and real analytic if $k=\omega$. A set $\mathfrak{A}$ of 
  pairwise $\shContFcts^k$-compatible charts of $M$ is called 
  a $\shContFcts^k$-\emph{atlas of} $M$ or an \emph{atlas of class} 
  $\shContFcts^k$ if the charts in $\mathfrak{A}$ cover $M$ which means that for each point $p\in M$ 
  there is a chart $(x,U)\in \mathfrak{A}$ with $p \in U$.
 
  Two complex charts 
  $x: U \rightarrow \widetilde{U} \subset \C^n$ and 
  $y: V \rightarrow \widetilde{V} \subset \C^m$ are called 
  \emph{holomorphically compatible}
  if the transition maps $x\circ y^{-1} : y(U\cap V) \to x(U\cap V) $ and 
  $y\circ x^{-1} : x(U\cap V) \to y(U\cap V) $ are both holomorphic. 
  A set $\mathfrak{A}$ of pairwise holomorphically compatible charts of $M$ is 
  a \emph{holomorphic atlas} of $M$ or an \emph{atlas of class} 
  $\shHolFcts$ if  the charts in $\mathfrak{A}$ cover $M$.
\end{definition}

\para The sets of  $\shContFcts^k$-atlases and of holomorphic atlases of a 
  topological space $M$ are ordered by set-theoretic inclusion. Obviously, if
  $\mathfrak{A}$ is an atlas of class  $\shContFcts^k$ or  $\shHolFcts$
  there exists a unique maximal atlas $\widehat{\mathfrak{A}}$ of the same class
  containing $\mathfrak{A}$. The atlas $\widehat{\mathfrak{A}}$ is obtained from 
  $\mathfrak{A}$ by adding all charts which are $\shContFcts^k$-compatible respectively 
  holomorphically compatible with each chart contained in  $\mathfrak{A}$.

  If $f: N \to M$ is a homeomorphism between two topological spaces $N$ and $M$, and 
  $\mathfrak{A}$ a maximal atlas on $M$,  the set of charts
  \[
   f^*\mathfrak{A}:= \big\{ \big(x\circ f_{|f^{-1}(U)},f^{-1}(U),\fldk^n\big) \mid (x,U,\fldk^n) \in \mathfrak{A} \big\}
  \]
  is a maximal atlas on $N$ and of the same differentiability class as $\mathfrak{A}$.
  In case $N$ comes  equipped with a maximal atlas $\mathfrak{B}$, too, the homeomorphism $f$ 
  is called a \emph{diffeomeorphism} from $(N,\mathfrak{B})$ to $(M,\mathfrak{A})$ if
  $f^*\mathfrak{A}$ coincides with  $\mathfrak{B}$. 
  If $\mathfrak{A}$ and  $\mathfrak{B}$ are two maximal atlases on $M$ and there exists a 
  homeomorphism $f : M \to M$ which is a diffeomorphism from $M$ equipped with $\mathfrak{A}$ 
  to $M$ equipped with $\mathfrak{B}$, 
  one calls the two atlases $\mathfrak{A}$ and  $\mathfrak{B}$ \emph{equivalent}. By definition
  it is clear that equivalence of maximal atlases on a topological space $M$ is an equivalence 
  relation indeed. 

\begin{definition}\label{def:locally-euclidean-space}
  A topological space $M$ is called \emph{locally euclidean}, if for
  every $p\in M$ there exists an open neighborhood $U$ together with a 
  homeomorphism $x : U \rightarrow \widetilde{U}$ mapping $U$ onto an 
  open subset $\widetilde{U}\subset \R^n$ of some euclidean space. 
\end{definition}
\begin{definition}\label{def:smooth-holomorphic-structures}
  Assume $M$ to be a topological space, and $k \in \N\cup \{ \infty, \omega\}$. 
  By  a $\shContFcts^k$-\emph{structure} or \emph{differentiable structure of class} 
  $\shContFcts^k$ on $M$ one understands a maximal  $\shContFcts^k$-atlas
  on $M$. A \emph{holomorphic structure} on $M$ is given by 
  a maximal holomorphic atlas.
  One sometimes calls a $\shContFcts^0$-structure a \emph{locally euclidean structure},
  a $\shContFcts^\infty$-structure a \emph{smooth structure}, and
  a $\shContFcts^\omega$-structure a (\emph{real}) \emph{analytic structure}
  on the underlying topological space $M$.
\end{definition}

\begin{remark}
\label{rem:locally-euclidean-smooth-structures}
  \begin{enumerate}[leftmargin=*]
  \item A topological space $M$ possesses a locally euclidean structure if and only 
  if the space is locally euclidean.
  \item 
   {\scshape John Milnor} showed in his paper  \ref{MilMH7S} 
   titled {\itshape On manifolds homeomorphic to the $7$-sphere}
   that the 7-dimensional sphere admits a 
   smooth structure which is not equivalent to the standard smooth structure defined in 
   \Cref{ex:standard-smooth-structure-sphere}. In other words this means that 

   Following {\scshape Milnor}, a topological space 
   equipped with a smooth structure which is homeomorphic but not diffeomeorphic to a standard 
   sphere is called  an \emph{exotic sphere}. 
   In joint work with {\scshape Michel Kervaire}, {\scshape Milnor} classified in \ref{KerMilGHS} 
   all smooth structures on the $7$-sphere up to equivalence and proved that there are 
   exactly $28$ of them.
  \end{enumerate}
\end{remark}