% Copyright 2016 - 2018  Ana Cannas da Silva and Alan Weinstein 
% main authors: 
%   Ana Cannas da Silva and Alan Weinstein
%
% Permission is granted to copy, distribute and/or modify this document under the terms of the 
% Creative Commons Attribution NonCommercial NoDerivatives 4.0 International License 
% or any later version published by the Creative Commons Corporation (“Creative Commons”) 
% A copy of the license are included in the folder licenses.
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GROUPOIDS
% original version by Ana Cannas da Silva and Alan Weinstein from November, 98
% need to add:
%   pictures of holonomy, Mobius strip and Reeb foliation
%   picture for groupoid embedding axiom?
%   explain notions of isotropy groups G_x of a groupoid, 
%   of G(y,x) := t^{-1}(y) \cap s^{-1} (x), and 
%   of source-fibers and target-fibers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{The category of groupoids}\label{sec:category-groupoids}

\para A groupoid can be thought of as a generalized group
in which only certain multiplications are possible. 
It can be consisely defined as a small category with all arrows invertible, 
but the following - logically equivalent - definition reflects the underlying concepts more clearly.


\begin{definition}
\label{def:groupoid}\index{groupoid ! definition}\index{groupoid}
A \emph{groupoid} over a set $X$ is a set $\grpdG$ together
with the following structure maps:
\begin{itemize}[leftmargin=*]
\item
 a pair of maps \( \grpdG  \overset{t}{\underset{s}{\rightrightarrows}} X \),
 where $t$ is called the \emph{target map}\index{groupoid ! target map}
 while $s$ is called the \emph{source map}\index{groupoid ! source map},
\item
 a (\emph{partial}) \emph{multiplication} or \emph{product}\index{groupoid ! product}\index{groupoid ! multiplication}
 $m:\grpdG^{(2)}\rightarrow \grpdG$, $(g,h) \mapsto gh$ defined on the set of
 \emph{composable pairs}\index{groupoid ! composable pairs}
 \[
  \grpdG^{(2)}:=\{ (g,h)\in \grpdG\times \grpdG \mid s (g)=t (h)\}\ ,
 \]
\item
 an embedding $u :X\rightarrow \grpdG$, called the \emph{identity section} or \emph{unit},
 \index{groupoid ! identity section}\index{groupoid ! unit} and 
\item
 an \emph{inversion map}\index{groupoid ! inversion map}
 $i: \grpdG\rightarrow \grpdG$, $g \mapsto g^{-1}$. 
\end{itemize}
These data must have the following properties:
\begin{axiomlist}[Grpd]
\item 
 Multiplication from the right preserves the target, multiplication from the left the source, 
 i.e.~$t (gh)=t(g)$ \& $s(gh)=s(h)$ for all $(g,h) \in \grpdG^{(2)}$.
\item Multiplication is associative, i.e.~$(gh)k=g(hk)$ for all $(g,h,k) \in \grpdG^{(3)}$, where 
 \[ \grpdG^{(3)} := \{ (g,h,k) \in \grpdG \times \grpdG \times \grpdG \mid s(g)=t(h), s(h)=t(k)\} \]
 denotes the set of \emph{composable triples}.
\item The unit map acts as identity, i.e.~$u (t(g))g=g=g u(s(g))$ for all $g\in \grpdG$.
      In particular, $t \circ u = s \circ u$ is the identity map on $X$.
\item The inversion map acts by inversion, i.e.~$i (g) g = u (s(g))$ \& $g i(g) = u (t(g))$ for all $g\in \grpdG$. 
\end{axiomlist}
 An element $g\in G$ is thought of as an arrow from the object $x=s(g)$ to the object $y=t(g)$:

\begin{picture}(150,50)(-60,40)
\put(20,50){\circle*{3}}
\put(-10,40){$y = t(g)$}
\put(130,50){\circle*{3}}
\put(133,40){$x = t(g)$}
\qbezier(127,53)(80,90)(25,53)
\put(80,80){$g$}
\thicklines
\put(29,56){\vector(-1,-1){5}}
\end{picture}

One therefore usually calls $\grpdG$ the \emph{arrow space}
of the groupoid, and $X$ its \emph{object space}. 
As already done so in the axioms, we will usually write $gh$ for the product $m(g,h)$. Whenever we write a product, 
we are assuming that it is defined. If $h$ is an arrow from
$x=s(h)$ to $y=t (h)=s (g)$ and $g$ is an arrow from $y$ to $z=t (g)$, then $gh$ is the composite arrow from $x$ to $z$:

\begin{picture}(200,100)(-60,0)
\put(0,40){\circle*{3}}
\put(-25,25){$t (g) = t (gh)$}
\put(100,40){\circle*{3}}
\put(77,25){$s (g)=t (h)$}
\put(200,40){\circle*{3}}
\put(180,25){$s (h) = s (gh)$}
\qbezier(3,43)(50,65)(97,43)
\qbezier(103,43)(150,65)(197,43)
\qbezier(5,50)(100,100)(195,50)
\put(50,42){$g$}
\put(150,42){$h$}
\put(100,80){$gh$}
\thicklines
\put(7,45){\vector(-2,-1){5}}
\put(107,45){\vector(-2,-1){5}}
\put(9,53){\vector(-1,-1){5}}
\end{picture}

The inversion map permutes source and target of an arrow:

\begin{picture}(150,100)(-75,-10)
\put(20,50){\circle*{3}}
\put(-35,35){$s (g^{-1}) = t (g)$}
\put(130,50){\circle*{3}}
\put(125,35){$s (g) = t (g^{-1})$}
\qbezier(125,55)(80,95)(25,55)
\qbezier[50](123,45)(80,5)(27,45)
\put(80,80){$g$}
\put(77,15){$g^{-1}$}
\thicklines
\put(28,57){\vector(-1,-1){5}}
\put(120,42){\vector(1,1){5}}
\end{picture}
\end{definition}

We will usually denote  a groupoid by \( \grpdG  \overset{t}{\underset{s}{\rightrightarrows}} X \), or
if we need to specify all structure maps by 
\[
 \grpdG^{(2)} \overset{m}{\rightarrow} \grpdG \overset{t}{\underset{s}{\rightrightarrows}} X \overset{u}{\rightarrow} \grpdG 
 \overset{i}{\rightarrow} \grpdG \: . 
\]
By an abuse of notation, we sometimes simply write $\grpdG$ for the groupoid above.

A groupoid $\grpdG$ gives rise to a hierarchy of sets:
\[
\begin{array}{ccl}
	\grpdG^{(0)} & := & u (X) \simeq X \\
	\grpdG^{(1)} & := & \grpdG \\
	\grpdG^{(2)} & := & \{ (g,h)\in \grpdG \times \grpdG  \mid s(g)= t(h)\} \\
	%\grpdG^{(3)} & := & \{ (g,h,k) \in \grpdG \times \grpdG \times \grpdG \mid
	% s(g)=t(h), s(h)=t(k)\} \\
	\vdots & &\\[1mm]
        \grpdG^{(n)} & := & \{ (g_1,\ldots ,g_n) \in \grpdG^n \mid
	 s(g_1)=t(g_2), \ldots , s(g_{n-1})=t(g_n)\}\\
	\vdots & &
\end{array}
\]

	The following picture can be useful in visualizing groupoids.

\begin{picture}(270,240)(-15,-25)
\put(0,90){\line(1,0){300}}
\multiput(-5,190)(30,0){8}{\line(1,-2){100}}
\multiput(-5,-10)(30,0){8}{\line(1,2){100}}
\put(135,150){\circle*{4}}
\put(125,150){$g$}
\put(180,120){\circle*{4}}
\put(185,120){$h$}
\put(135,30){\circle*{4}}
\put(117,25){$g^{-1}$}
\put(150,180){\circle*{4}}
\put(155,180){$gh$}
\put(2,155){$s$-fibers}
\put(250,155){$t$-fibers}
\put(165,90){\circle*{4}}
\put(140,80){$s (g)=t (h)$}
\put(-10,93){$\grpdG^{(0)} \simeq X$}
\thicklines
\put(135,150){\line(1,-2){30}}
\put(150,180){\line(1,-2){30}}
\put(135,150){\line(1,2){15}}
\put(165,90){\line(1,2){15}}
\end{picture}

\begin{remark}
There are various equivalent definitions for groupoids and
various ways of thinking of them.
As already pointed ourt above, a groupoid $G$ can be viewed as
a small category whose objects are the elements of
the base set $X$ and whose morphisms are all invertible.
Another way to think of a groupoid is as a
generalized equivalence relation in which elements of $X$ can be
``equivalent in several ways'' (see Paragraph~\ref{para:groupoid-generalized-equivalence-relation}).
We refer to Brown~\cite{br:groups,BroTG},
as well as~\cite{we:unifying}, for extensive general discussion of groupoids.
\end{remark}



\begin{examples}
\begin{enumerate}[leftmargin=*]
\item
 A \emph{group}\index{group ! definition} is a groupoid over
 a set $X = \{ \ast \} $ with only one element.
\item
 The \emph{trivial groupoid}\index{groupoid ! trivial}
 over the set $X$ is defined by $\grpdG=X$,
 and $t =s = \id_X$.
\item
 Let $\grpdG=X\times X\overset{\operatorname{pr}_1}{\underset{\operatorname{pr}_2}{\rightrightarrows}} X$ 
 with the groupoid structure defined by
 \begin{equation*}
 \begin{split}
  t (y,x) & := \operatorname{pr}_2 (y,x) =  y, \quad s (y,x) := \operatorname{pr}_1 (y,x) = x, \\
  m \big( (z,y), (y,x) \big) & := (z,y)(y,x)  := (z,x) ,  \\
  u (x) &  :=(x,x) ,  \text{ and } i(y,x) : = (y,x)^{-1} :=  (x,y) \ .
 \end{split}
 \end{equation*}
  

This is often called the \emph{pair groupoid}\index{pair
groupoid}\index{groupoid ! pair groupoid}, or the
\emph{coarse groupoid}\index{coarse groupoid}\index{groupoid !
coarse groupoid}, or the \emph{Brandt groupoid}\index{groupoid !
Brandt groupoid}\index{Brandt groupoid} after work of
Brandt~\cite{br:uber}\index{Brandt, W.}, who is
generally credited with introducing the groupoid concept.

\begin{picture}(170,130)(-90,0)
\multiput(0,20)(0,20){5}{\line(1,0){100}}
\multiput(20,0)(20,0){5}{\line(0,1){100}}
\put(-15,80){$X$}
\put(80,-15){$X$}
\put(73,65){$\varepsilon (X)$}
\put(110,40){\line(0,-1){30}}
\put(40,110){\line(-1,0){30}}
\put(114,25){$\pi_1$}
\put(25,114){$\pi_2$}
\put(110,12){\vector(0,-1){5}}
\put(12,110){\vector(-1,0){5}}
\thicklines
\put(0,0){\line(1,0){100}}
\put(0,0){\line(0,1){100}}
\put(0,0){\line(1,1){100}}
\end{picture}
\end{enumerate}
\end{examples}



\begin{remark}
Given a groupoid $G$, choose some $\phi\not\in G$.  The groupoid
multiplication on $G$ extends to a multiplication on the set
$G \cup \{ \phi \}$ by
\[
\begin{array}{l}
	g\phi =\phi g=\phi \\
	gh=\phi\ , \quad \quad \mbox{ if $(g,h) \in
	(G\times G)\setminus \grpdG^{(2)}$}\ .
\end{array}
\]
The new element $\phi$ acts as a ``receptacle'' for any previously
undefined product.  This endows $G \cup \{ \phi \}$ with a
\emph{semigroup}\index{semigroup}
structure.  A groupoid thus becomes a special kind of semigroup as well.
\end{remark}


\begin{definition}
Given two groupoids $\grpdG_1$ and $\grpdG_2$ over sets $X_1$ and $X_2$
respectively, a
\emph{morphism of groupoids}\index{groupoid ! morphism}\index{morphism
of groupoids} is a pair of maps $\grpdG_1\rightarrow \grpdG_2$ and
$X_1\rightarrow X_2$ which commute with all the structural functions of
$G_1$ and $G_2$.
We depict a morphism by the following diagram.
\[
\begin{diagram}
	G_1 & \rTo & G_2 \\
	\dTo^{t_1}\dTo_{s_1} & & \dTo^{t_2}\dTo_{s_2} \\
	X_1 & \rTo & X_2
\end{diagram}
\]
If we consider a groupoid as a special type of category, then a morphism
between groupoids is simply a covariant functor between the categories.  
\end{definition}

There is a natural way to form the
\emph{product of groupoids}\index{groupoid ! product of
groupoids}\index{product ! of groupoids}:
 
\begin{remark}
	If $G_i$ is a groupoid over $X_i$ for $i=1,2$, show that there is a
naturally defined direct product groupoid $G_1\times G_2$ over $X_1\times X_2$.

\emph{disjoint union} of groupoids is a groupoid.
\end{remark}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsec{Subgroupoids and Orbits}\label{subgroupoids}
\index{groupoid ! subgroupoid}\index{subgroupoid ! definition}

\begin{definition}
A subset $\grpd{H}$ of a groupoid $\grpdG$ over $X$ is called a \emph{subgroupoid}
if it is closed under multiplication (when defined) and inversion.
Note that
\[
	h \in \grpd{H} \Rightarrow h^{-1}\in \grpd{H} \Rightarrow
	\text{ both } u (t (h))\in \grpd{H}
	\text{ and } u (s (h))\in \grpd{H}\ .
\]
Therefore, the subgroupoid $\grpd{H}$ is a groupoid over $t (\grpd{H})=s (\grpd{H})$,
which may or may not be all of $X$.  When $t (\grpd{H})=s (\grpd{H})=X$, $\grpd{H}$ is
called a \emph{wide subgroupoid}\index{groupoid ! wide subgroupoid}\index{subgroupoid ! wide}.
\end{definition}

\begin{examples}
\begin{enumerate}[leftmargin=*]
\item
 If $\grpdG=X$ is the trivial groupoid, then any subset of $\grpdG$ is a
 subgroupoid, and the only wide subgroupoid is $\grpdG$ itself.
\item
 If $X =\{ \ast\}$ is a one point set, so that $\grpdG$ is a group, then the
 nonempty subgroupoids are the subgroups of $\grpdG$, but the empty set is also
 a subgroupoid of $\grpdG$.
\item
 If $\grpdG=X\times X$ is the pair groupoid, then a subgroupoid $\grpd{H}$ is a 
 \emph{relation}\index{subgroupoid ! as a relation}\index{groupoid ! relation}\index{relation}
 on $X$ which is \emph{symmetric} and \emph{transitive}.  A wide
 subgroupoid $\grpd{H}$ is an \emph{equivalence relation}.  In general, $\grpd{H}$ is an
 equivalence relation on the set $t (\grpd{H})=s (\grpd{H}) \subset X$.
\end{enumerate}
\end{examples}


\para\label{para:groupoid-generalized-equivalence-relation}
For any groupoid $G$ over a set $X$, there is a morphism
\[
\begin{diagram}
	\phantom{x} G \phantom{x} & \rTo^{(t ,s )} & X\times X \\
	\dTo^t \dTo_s & & \dTo^{\pi_1}\dTo_{\pi_2} \\
	X & = & X
\end{diagram}
\]
from $G$ to the pair groupoid over $X$.  Its image is a wide subgroupoid
of $X\times X$, and hence defines an equivalence relation on $X$.
The equivalence classes are called the \emph{orbits}\index{groupoid !
orbit}\index{orbit ! groupoid} of $G$ in $X$.
In category language, the orbits are the isomorphism classes of the
objects of the category.
We can also think of a groupoid as an equivalence
relation where two elements might be equivalent in different ways,
parametrized by the kernel of $(t ,s )$.
The groupoid further indicates the
structure of the set of all ways in which two elements are equivalent.

	Inside the groupoid $X\times X$ there is a
\emph{diagonal subgroupoid}\index{groupoid ! diagonal
subgroupoid}\index{subgroupoid ! diagonal}\index{diagonal subgroupoid}
$\Delta =\{ (x,x) \mid x \in X\}$.
We call $(t ,s )^{-1}(\Delta )$ the
\emph{isotropy subgroupoid}\index{groupoid ! isotropy
subgroupoid}\index{subgroupoid ! isotropy}\index{isotropy ! subgroupoid}
of $G$.
\[
	(t ,s )^{-1}(\Delta ) 
	= \{ g\in G \mid t (g)=s (g)\}
	= {\displaystyle \bigcup_{x\in X}G_x}\ , 
\]
where $G_x := \{ g \mid t (g)=s (g)=x\}$ is the
\emph{isotropy subgroup}\index{group ! isotropy
subgroup}\index{subgroup ! isotropy}\index{isotropy ! subgroup}
of $x$.

	If $x,y\in X$ are in the same orbit, then any element $g$ of
\[
	G_{x,y}:=(t ,s )^{-1}(x,y) =
	\{ g \in G  \mid  t (g) = x \mbox{ and } s (g) = y \}
\]
induces an isomorphism $h \mapsto g^{-1} h g$ from $G_x$ to $G_y$.
On the other hand, the groups $G_x$ and $G_y$ have natural commuting, free
transitive actions on $G_{x,y}$, by left and right multiplication,
respectively.
Consequently, $G_{x,y}$ is isomorphic (as a set) to
$G_x$ (and to $G_y$), but not in a natural way.

	A groupoid is called \emph{transitive}\index{groupoid !
transitive}\index{transitive ! groupoid} if it has just one orbit.
The transitive groupoids are the building blocks of groupoids, in the
following sense.  There is a natural decomposition of the base space
of a general groupoid into orbits.
Over each orbit there is a transitive groupoid, and the disjoint
union of these transitive groupoids is the original groupoid.

\begin{historical}
	Brandt~\cite{br:uber}\index{Brandt, W.} discovered groupoids
while studying quadratic forms over the integers.
Groupoids also appeared in Galois theory\index{groupoid !
Galois theory} in the description of
relations between subfields of a field $K$ via morphisms
of $K$~\cite{lo:neue}.
The isotropy groups of the constructed groupoid turn out to be the Galois
groups.  Groupoids occur also as generalizations of equivalence relations
in the work of Grothendieck\index{Grothendieck, A.} on moduli
spaces~\cite{gr:techniques}
and in the work of Mackey\index{Mackey, G.}
on ergodic theory~\cite{ma:ergodic}\index{groupoid ! ergodic theory}.
For recent applications in these two areas, see Keel\index{Keel, S.} and
Mori~\cite{ke-mo:quotients}\index{Mori, S.} and
Connes~\cite{co:noncommutative}\index{Connes, A.}.
\end{historical}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsec{Examples of Groupoids}
\index{groupoid ! examples}\index{examples ! groupoids}

\begin{example}[Fundamental groupoid]
Let $X$ be a topological space and let $\grpdG = \pi_1 (X)$ be the collection of
homotopy classes of continuous paths in $X$ with all possible fixed endpoints.
Specifically, if $\gamma :[0,1]\rightarrow X$ is a continuous path from $x=\gamma(0)$
to $y=\gamma(1)$, let $[\gamma ]$ denote the homotopy class of $\gamma$
relative to the points $x,y$.  We can define a groupoid
\[
	\pi_1 (X) = \{ (x,[\gamma ],y) \mid x,y\in X,\gamma 
	\text{ is a continuous path from $x$ to $y$}\}\ ,
\]
where multiplication is concatenation of paths. According to our convention, if $\gamma$ is a path from
$x$ to $y$, the target is $t (x,[\gamma ],y) = x$ and the source
is $s (x,[\gamma ],y) = y$.)  The groupoid $\pi_1 (X)$ is called
the \emph{fundamental groupoid}\index{groupoid ! fundamental}\index{fundamental groupoid} of
$X$.  The orbits of $\pi_1 (X)$ are just the path components of $X$.
In fact, for a general groupoid, we will occasionally refer
to the orbits as \emph{components}\index{groupoid ! components}\index{components}.
See Brown's text on algebraic
topology~\cite{BroTG} for more on fundamental groupoids.

There are several advantages of the fundamental groupoid over the
fundamental group.  First notice that the fundamental group sits within
the fundamental groupoid as the isotropy subgroup over a single point.
The fundamental groupoid does not require a choice of base point and is
better suited to study spaces that are not path connected.
Additionally, many of the algebraic properties of the fundamental group
generalize to the fundamental groupoid, as illustrated in the following
result.
\end{example}

\begin{remark}
	Show that the Seifert-Van Kampen theorem on the fundamental group
of a union $U \cup V$ can be generalized to
groupoids~\cite{br:topology}, and that the connectedness condition on
$U \cap V$ is then no longer necessary.
\end{remark}

\begin{example}
Let $\Gamma$ be a group acting on a space $X$.  In the
product groupoid $\Gamma\times (X\times X)\simeq X\times\Gamma\times X$ over
$\{ \mbox{point} \} \times X \simeq X$, the wide subgroupoid
\[
	G_\Gamma =\{ (x,\gamma ,y) \mid x = \gamma \cdot y \}
\]
is called the \emph{transformation groupoid}\index{groupoid !
transformation groupoid}\index{transformation ! groupoid} or
\emph{action groupoid}\index{groupoid !
action}\index{action ! groupoid} of the $\Gamma$-action.
The orbits and isotropy subgroups of the
transformation groupoid are precisely those of the $\Gamma$-action.

	A groupoid $G$ over $X$ is called \emph{principal}\index{groupoid !
principal groupoid}\index{principal groupoid} if the morphism
$G\stackrel{(t ,s )}{\longrightarrow}X\times X$ is injective.
In this case, $G$ is isomorphic to the image $(t ,s ) (G)$,
which is an equivalence relation on $X$.
The term ``principal'' comes from the analogy with bundles
over topological spaces.

	If $\Gamma$ acts freely on $X$, then the transformation groupoid
$G_\Gamma$ is principal, and $(t ,s ) (G_\Gamma)$ is the orbit
equivalence relation on $X$.  In passing to the transformation groupoid,
we have lost information on the group structure of $\Gamma$, as we
no longer see {\em how} $\Gamma$ acts on the orbits: different free group
actions could have the same orbits.
\end{example}

\begin{example}
	Let $\Gamma$ be a group.  There is an interesting ternary operation
\[
	(x,y,z)\stackrel{t}{\longrightarrow}xy^{-1}z\ .
\]
It is invariant under left and right translations (check this as an
exercise), and it defines 4-tuples $(x,y,z,xy^{-1}z)$ in $\Gamma$
which play the role of parallelograms.
The operation $t$ encodes the affine structure of the group in the sense that,
if we know the identity element $e$, we recover the group operations
by setting $x=z=e$ to get the inversion and then $z=e$ to get the
multiplication.  However, the identity element of $\Gamma$ cannot be
recovered from $t$.\end{example}

	Denote
\[
\begin{array}{rcl}
	\mathfrak S(\Gamma ) &=& \mbox{set of subgroups of $\Gamma$} \\
	\FB (\Gamma ) &=& \mbox{set of subsets of $\Gamma$ closed under $t$}\ .
\end{array}
\]

\begin{proposition}
	$\FB (\Gamma )$ is the set of {\em cosets} of elements of
$\mathfrak S(\Gamma)$.
\end{proposition}

	The sets of right and of left cosets of subgroups of
$\Gamma$ coincide because $g H = (g H g^{-1}) g$, for any $g \in G$ and
any subgroup $H \leq G$.

\begin{exercise}
	Prove the proposition above.
\end{exercise}

	We call $\FB (\Gamma )$ the \emph{Baer groupoid}\index{groupoid !
Baer groupoid}\index{Baer groupoid} of $\Gamma$,
since much of its structure was formulated by
Baer~\cite{ba:scharbegriffs}\index{Baer, A.}.
We will next see that the Baer groupoid is a groupoid over
$\mathfrak S(\Gamma )$.

	For $D\in\FB (\Gamma )$, let $t (D)=g^{-1}D$ and
$s (D)=Dg^{-1}$ for some $g\in D$.
From basic group theory,
we know that $t$ and $s$ are maps into $\mathfrak S(\Gamma )$
and are independent of the choice of $g$.
Furthermore, we see that
$s (D) = gt (D)g^{-1}$ is conjugate to $t (D)$.

\[
\begin{array}{c}
	\FB (\Gamma ) \\
	{\scriptstylet}\downarrow\downarrow{\scriptstyles} \\
	\mathfrak S(\Gamma )
\end{array}
\]

\begin{exercise}
	Show that if $s (D_1)=t (D_2)$, {\em i.e.}\ 
$D_1 g_1^{-1} = g_2^{-1} D_2$ for any $g_1 \in D_1, g_2 \in D_2$, then the
product in this groupoid can be defined by
\[
	D_1D_2 := g_2 D_1 = g_1 D_2 = \{ gh \mid g\in D_1\; h\in D_2\}\ .
\]
\end{exercise}

	Observe that the orbits of $\FB (\Gamma )$ are the conjugacy
classes of subgroups of $\Gamma$.  In particular, over a single conjugacy
class of subgroups is a transitive groupoid, and thus we see that the Baer
groupoid is a refinement of the conjugacy relation on subgroups.

	The isotropy subgroup of a subgroup $H$ of $\Gamma$ consists of all
left cosets of $H$ which are also right cosets of $H$.  Any left
coset $gH$ is a right coset $(gHg^{-1})g$ of $gHg^{-1}$.  Thus $gH$ is also
a right coset of $H$ exactly when $gHg^{-1}=H$, or, equivalently,
when $s (gH)=t (gH)$.  Thus the isotropy subgroup of $H$ can be 
identified with $N(H)/H$, where $N(H)$ is the normalizer of $H$.
\end{example}

\begin{example}
Let $\Gamma$ be a compact connected semisimple Lie group.
An interesting conjugacy class of subgroups of $\Gamma$ is
\[
	\cT =\{\mbox{maximal tori of $\Gamma$}\}\ ,
\]
where a \emph{maximal torus}\index{maximal torus}\index{torus !
maximal} of $\Gamma$ is a subgroup 
\[
	\torus^k \simeq (S^1)^k = S^1 \oplus \cdots \oplus S^1
\]
of $\Gamma$ which is maximal in the sense that there does not exist an
$\ell\geq k$ such that $\torus^k<\torus^\ell\leq \Gamma$ (here,
$S^1 \simeq \RR / \ZZ$ is the circle group).
A theorem from Lie group theory (see, for instance,~\cite{br-di:book})
states that any two maximal tori of a connected
Lie group are conjugate, so $\cT$ is an orbit of $\FB (\Gamma)$.
We call the transitive subgroupoid
$\FB (\Gamma)|_\cT =\mathcal W(\Gamma)$ the
\emph{Weyl groupoid}\index{Weyl groupoid}\index{groupoid !
Weyl groupoid} of $\Gamma$. 
\end{example}


\begin{remarks}
\begin{itemize}

\item
	For any maximal torus $\torus \in \cT$, the quotient
$N(\torus )/\torus$ is the classical Weyl group\index{Weyl group}.
The relation between the Weyl groupoid and
the Weyl group is analogous to the relation between the fundamental groupoid
and the fundamental group.

\item
	There should be relevant applications of Weyl groupoids in the
representation
theory of a group $\Gamma$ which is acted on by a second group, or in studying
the representations of groups that are not connected.
\end{itemize}
\end{remarks}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsec{Groupoids with Structure}
\index{groupoid ! with structure}
\labell{groupoid-structure}
\medskip

	Ehresmann~\cite{eh:oeuvres}\index{Ehresmann, C.} was the
first to endow groupoids with additional structure,
as he applied groupoids to his study of foliations.
Rather than attempting to describe a general theory of
``structured groupoids,'' we will simply mention some useful special cases.

\medskip
\begin{enumerate}

\item
	\emph{Topological groupoids: }\index{groupoid !
topological}\index{topological groupoid}
For a topological groupoid, $G$ and $X$ are required to be topological
spaces and all the structure maps must be continuous.  

\begin{examples}
\begin{itemize}
\item
In the case of a group, this is the same as the concept of topological group.
\item
The pair groupoid of a topological space has a natural topological structure
derived from the product topology on $X\times X$.
\end{itemize}
\end{examples}

	For analyzing topological groupoids, it is useful to impose
certain further axioms
on $G$ and $X$.  For a more complete discussion, see~\cite{re:groupoid}.
Here is a sampling of commonly used axioms:
\begin{enumerate}
\item
$\grpdG^{(0)} \simeq X$ is locally compact and Hausdorff.
\item
The $t$- and $s$-fibers are locally compact and Hausdorff.
\item
There is a countable family of compact Hausdorff subsets of $G$ whose
interiors form a basis for the topology.
\item
$G$ admits a \emph{Haar system}\index{Haar system}, that is, admits a family
of measures on the $t$-fibers which is invariant under left translations.
For any $g\in G$, left translation by $g$ is a map between $t$ fibers
\[
\begin{array}{rcl}
	t^{-1}(s (g)) & \longrightarrow & t^{-1}(t (g))\\
	h & \stackrel{\ell_g}\longmapsto & gh\ . 
\end{array}
\]

\begin{picture}(250,120)(-25,-35)
\put(0,0){\line(1,0){220}}
\multiput(10,-20)(80,0){2}{\line(1,1){80}}
%\put(110,40){\line(1,-1){40}}
\lixo{redraw lines as dashed, not dotted}
\qbezier[40](110,40)(130,20)(150,0)
\qbezier[40](120,50)(140,30)(160,10)
\put(110,40){\circle*{4}}
\put(100,40){$g$}
\put(120,50){\circle*{4}}
\put(125,48){$\ell_g (h)$}
\put(150,0){\circle*{4}}
\put(150,-10){$s (g)$}
\put(160,10){\circle*{4}}
\put(167,7){$h$}
\put(195,65){$t ^{-1} (s (g))$}
\put(115,65){$t ^{-1} (t (g))$}
\thicklines
\multiput(50,-20)(80,0){2}{\line(1,1){80}}
\end{picture}

\begin{example}
	For the pair groupoid, each fiber can be identified with the base
space $X$.
A family of measures is invariant under translation
if and only if the measure is the same on each fiber.
Hence, a Haar system on a pair groupoid corresponds to a measure on $X$.
\end{example}
\end{enumerate}

\item
	\emph{Measurable groupoids: }\index{groupoid !
measurable}\index{measurable groupoid}
These groupoids, also called \emph{Borel groupoids}\index{groupoid !
Borel}\index{Borel groupoid},
come equipped with a $\sigma$-algebra of sets and a
distinguished subalgebra (called the null sets);
see~\cite{ma:ergodic,mo-sc:global_analysis}.
On each $t$-fiber,
there is a \emph{measure class},\index{measure ! class} which is simply
a measure defined up to multiplication by an invertible measurable function.

\item
	\emph{Lie groupoids}\index{groupoid ! Lie}
\index{Lie groupoid ! definition} or \emph{differentiable
groupoids: }\index{groupoid !
differentiable}\index{differentiable groupoid}
The groupoid $G$ and the base space $X$ are manifolds and
all the structure maps are smooth.
It is {\em not} assumed that $G$
is Hausdorff, but only that $\grpdG^{(0)} \simeq X$ is a Hausdorff manifold
and closed in $G$.\footnote{Throughout these notes, a manifold is
assumed to be Hausdorff, {\em unless} it is a groupoid.}
Thus we can require that the identity section be smooth.
Recall that multiplication is defined as a map on $\grpdG^{(2)}\subseteq G$.
To require that multiplication
be smooth, first $\grpdG^{(2)}$ needs to be a smooth manifold.
It is convenient to make the stronger assumption
that the map $t$ (or $s$) be a submersion.

\begin{exercise}
	Show that the following conditions are equivalent:
\begin{itemize}
\item[(a)]
	$t$ is a submersion,

\item[(b)]
	$s$ is a submersion,

\item[(c)]
	the map $(t ,s )$ to the pair groupoid
is transverse to the diagonal.
\end{itemize}
\end{exercise}

\lixo{	Note that $t$ is the identity
map along $\grpdG^{(0)}$ and is thus automatically a submersion near $\grpdG^{(0)}$.

\begin{exercise}
	Show that $t$ is a submersion on an open and closed subset of $G$.
\end{exercise}

	Thus, we could drop the submersion assumption on $t$,
by assuming that $G$ is connected.}

\item
\emph{Bundles of groups: }\index{groupoid !
bundle of groups}\index{bundle ! of groups}
A groupoid for which $t =s$ is called a bundle of groups.
This is not necessarily a trivial bundle, or even a locally trivial bundle
in the topological case,
as the fibers need not be isomorphic as groups or as topological spaces.
The orbits are the individual points of the base space, and the isotropy
subgroupoids are the fiber groups of the bundle.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsec{The Holonomy Groupoid of a Foliation}
\index{groupoid ! holonomy of a foliation}
\index{foliation ! holonomy groupoid}
\index{holonomy ! groupoid of a foliation}
\medskip

	Let $X$ be a (Hausdorff) manifold.
Let $F\subseteq TX$ be an integrable subbundle, and $\cF$ the
corresponding foliation ($\cF$ is the decomposition
of $X$ into maximal integral manifolds called
\emph{leaves}\index{leaf ! definition}\index{foliation ! leaf}).
The notion of holonomy\index{holonomy ! description} can be
described as follows. 
An $F$-path is a path in $X$ whose tangent vectors lie within $F$.
Suppose that $\gamma :[0,1]\rightarrow\cO$ is an
$F$-path along a leaf $\cO$.
Let $N_{\gamma(0)}$ and $N_{\gamma(1)}$
be cross-sections for the spaces of leaves near $\gamma(0)$ and $\gamma(1)$,
respectively, {\em i.e.}\ they are two small transversal manifolds to
the foliation at the end points of $\gamma$.  There is an
$F$-path near $\gamma$ from each point near $\gamma (0)$
in $N_{\gamma(0)}$ to a uniquely determined point in
$N_{\gamma(1)}$.  This defines a local
diffeomorphism between the two leaf spaces.
The \emph{holonomy}\index{holonomy ! definition}
of $\gamma$ is defined to be the {\em germ}, or direct
limit, of such diffeomorphisms, between the local leaf spaces
$N_{\gamma(0)}$ and $N_{\gamma(1)}$.

	The notion of holonomy allows us to define an equivalence relation
on the set of $F$-paths from $x$ to $y$ in $X$.
Let $[\gamma ]_{_H}$ denote the
equivalence class of $\gamma$ under the relation that two paths are
equivalent if they have the same holonomy\index{holonomy !
equivalence relation}.  

	The \emph{holonomy groupoid}\index{holonomy !
groupoid of a foliation}~\cite{co:noncommutative},
also called the \emph{graph}\index{foliation ! graph}\index{graph}
of the foliation~\cite{wi:graph}, is
\[
	H(\cF )=\{ (x,[\gamma ]_{_H},y) \mid x,y\in X,\gamma
	\mbox{ is an $F$-path from $x$ to $y$}\}\ .
\]

	Given a foliation $\cF$, there are two other related groupoids
obtained by changing the equivalence relation on paths:

\begin{enumerate}

\item
	The \emph{$\cF$-pair groupoid}\index{groupoid ! pair
groupoid}\index{pair groupoid} -- This groupoid is the
equivalence relation for which the equivalence classes
are the leaves of $\cF$, {\em i.e.}\ we consider any two $F$-paths
between $x,y\in \cO$ to be equivalent.

\item
	The \emph{$\cF$-fundamental groupoid}\index{groupoid !
fundamental groupoid}\index{fundamental groupoid} -- For this groupoid,
two $F$-paths
between $x,y$ are equivalent if and only if they are
\emph{$F$-homotopic}\index{F-homotopic@$F$-homotopic}\index{foliation !
F-homotopy@$F$-homotopy}, that is, homotopic within the
set of all $F$-paths.  Let $[\gamma]_{_F}$ denote the equivalence class
of $\gamma$ under $F$-homotopy.  The set of this groupoid is
\[
	\Pi (\cF) = \{ (x,[\gamma ]_{_F},y) \mid
	x,y\in X,\gamma\mbox{ is an $F$-path from $x$ to $y$}\}\ .
\]

\end{enumerate}

	If two paths $\gamma_1,\gamma_2$ are $F$-homotopic
with fixed endpoints, then they give the same holonomy,
so the holonomy groupoid is intermediate between the $\cF$-pair groupoid
and the $\cF$-fundamental groupoid:
\[
	[\gamma_1 ]_{_F} = [\gamma_2 ]_{_F}
	\quad \Longrightarrow \quad
	[\gamma_1 ]_{_H} = [\gamma_2 ]_{_H}\ .
\]
The pair groupoid may not be a manifold.
With suitably defined differentiable structures, though, we have:

\begin{thm}
	$H(\cF )$ and $\Pi(\cF )$ are (not necessarily Hausdorff)
Lie groupoids.
\end{thm}

	For a nice proof of this theorem, and a comparison of the two
groupoids, see~\cite{ph:imperative}.
Further information can be found in~\cite{la:groupoide}.

\begin{exercise}
	Compare the $\cF$-pair groupoid, the holonomy groupoid of $\cF$,
and the $\cF$-fundamental groupoid for the
M\"obius band and the Reeb foliation, as described below.
\end{exercise}

\begin{enumerate}

\item
	The \emph{M\"obius band}\index{M\"obius band}. 
Take the quotient of the unit square
$[0,1]\times [0,1]$ by the relation $(1,x)\sim (0,1-x)$.  Define the leaves
of $\cF$ to be images of
the horizontal strips $\{ (x,y) \mid y=\mbox{constant}\}$.

\item
	The \emph{Reeb foliation}~\cite{re:foliation}\index{Reeb
foliation}\index{foliation ! Reeb foliation}\index{Reeb, G.}.
Consider the family of curves $x=c+\sec y$ on
the strip $-\pi/2 < y < \pi/2$ in the $xy$-plane.
If we revolve about
the axis $y=0$, then this defines a foliation of the solid cylinder by planes.
Noting that the foliation is invariant under translation, we see that this
defines a foliation of the open solid torus $D^2\times S^1$ by planes.
The foliation is smooth because its restriction to the $xy$-plane
is defined by the 1-form $\cos ^2 y \, dx + \sin y \, dy$,
which is smooth even when $y = \pm \frac{\pi}{2}$.
We close the solid torus by adding one exceptional leaf --
the $\torus^2$ boundary.  

	Let $t$ be a vanishing cycle on $\torus^2$, that is,
$[t ]\in\pi_1(\torus^2)$ generates the kernel of the natural map
$\pi_1(\torus^2)\rightarrow\pi_1(D^2\times S^1)$.
Although
$t$ is not null-homotopic on the exceptional leaf, any perturbation of
$t$ to a nearby leaf results in a curve that is $F$-homotopically
trivial.  On the other hand, the transverse curve (the cycle given by
$(c,y)\in D^2\times S^1$ for some fixed $c\in\partial D^2$) cannot be
pushed onto any of the nearby leaves.

	A basic exercise in topology shows us that we can glue two solid tori
together so that the resulting manifold is the 3-sphere $S^3$.
For this gluing, the transverse cycle of one torus is the vanishing cycle
of the other.  (If we instead glued the two vanishing cycles and the two
transverse cycles together, we would obtain $S^2 \times S^1$.)

	It is interesting to compute the holonomy on each side of the gluing
$\torus^2$.  Each of the two basic cycles in $\torus^2$
has trivial holonomy on one of its sides (holonomy given by the germ
of the identity diffeomorphism), and {\em non}-trivial holonomy on the
other side (given by the germ of an expanding diffeomorphism).

\begin{picture}(180,150)(-60,-7)
\multiput(10,87)(0,7){4}{\line(1,0){160}}
\put(177,80){$\gamma \mbox{ inside }\torus^2$}
\put(180,110){$N_{\gamma(1)}$}
\put(-20,110){$N_{\gamma(0)}$}
\put(10,10){\line(0,1){110}}
\put(170,10){\line(0,1){110}}
\qbezier(10,73)(140,70)(170,65)
\qbezier(10,66)(140,60)(170,50)
\qbezier(10,59)(140,50)(170,35)
\qbezier(10,52)(140,40)(170,20)
\thicklines
\put(10,80){\line(1,0){160}}
\end{picture}

	This provides an example of
{\em one-sided holonomy}\index{one-sided holonomy}\index{holonomy !
one-sided}\index{foliation !
one-sided holonomy}, a phenomenon that cannot happen for real analytic maps.
The leaf space of this foliation is not
Hausdorff; in fact, any function constant on the leaves must be constant
on all of $S^3$, since all leaves come arbitrarily close to the exceptional
leaf $\torus^2$.
This foliation and its holonomy
provided the inspiration for the following theorems.

\begin{thm}[Haefliger~\cite{ha:groupoides}]\index{Haefliger, A.}
	$S^3$ has no real analytic foliation of codimension-1.
\end{thm}

\begin{thm}[Novikov~\cite{no:foliations}]\index{Novikov, S.}
	Every codimension-1 foliation of $S^3$ has a compact leaf
that is a torus.
\end{thm}

\end{enumerate}

